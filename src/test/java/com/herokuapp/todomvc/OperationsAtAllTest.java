package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import com.herokuapp.todomvc.testconfigs.BaseTest;
import org.junit.Test;

import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.*;
import static com.herokuapp.todomvc.pages.ToDoMVC.aTask;

/**
 * Created by aru on 2017-03-28.
 */
public class OperationsAtAllTest extends BaseTest{
   
    @Test
    public void testEdit() {
        ToDoMVC.given(aTask(ACTIVE, "1"));

        ToDoMVC.edit("1", "1_edited");
        ToDoMVC.assertTasksAre("1_edited");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDelete() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.delete("2");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEmptying() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.edit("2", "");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testCompleteAll() {
        ToDoMVC.given(aTask(ACTIVE, "1"), aTask(COMPLETED, "2"));

        ToDoMVC.toggleAll();
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(0);
    }

    @Test
    public void testFilteringActiveToAll() {
        ToDoMVC.givenAtActive(aTask(COMPLETED, "1"), aTask(ACTIVE, "2"));

        ToDoMVC.filterAll();
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testClearCompleted() {
        ToDoMVC.given(aTask(COMPLETED, "1", "2"));

        ToDoMVC.clearCompleted();
        ToDoMVC.assertNoTasks();
        ToDoMVC.assertNoFooter();
    }

    @Test
    public void testCancelEdit() {
        ToDoMVC.given(aTask(ACTIVE, "1", "2"));

        ToDoMVC.cancelEdit("1", "1_edited");
        ToDoMVC.assertTasksAre("1", "2");

    }

    @Test
    public void testReopen() {
        ToDoMVC.given(aTask(COMPLETED, "1", "2"));

        ToDoMVC.toggle("2");
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(1);
    }
}
