package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import com.herokuapp.todomvc.testconfigs.BaseTest;
import org.junit.Test;



/**
 * Created by aru on 2017-03-28.
 */
public class LifecycleTest extends BaseTest{
    
    @Test
    public void testTaskManagement() {
        ToDoMVC.given();

        ToDoMVC.add("1");
        ToDoMVC.toggle("1");//completeAtAll
        ToDoMVC.assertTasksAre("1");

        ToDoMVC.filterActive();//All->Active
        ToDoMVC.assertNoTasks();
        ToDoMVC.add("2");
        ToDoMVC.assertTasksAre("2");
        ToDoMVC.toggleAll();//completeAllAtActive
        ToDoMVC.assertNoTasks();

        ToDoMVC.filterCompleted();//Active->Completed
        ToDoMVC. assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(0);
        ToDoMVC.toggle("2");//reopenAtCompleted
        ToDoMVC.clearCompleted();
        ToDoMVC.assertNoTasks();

        ToDoMVC.filterAll();//Completed->All
        ToDoMVC.assertTasksAre("2");
    }
}
