package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import com.herokuapp.todomvc.testconfigs.BaseTest;
import org.junit.Test;

import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.*;
import static com.herokuapp.todomvc.pages.ToDoMVC.aTask;

/**
 * Created by aru on 2017-03-28.
 */
public class OperationsAtCompletedTest extends BaseTest{
   
    @Test
    public void testEdit() {
        ToDoMVC.givenAtCompleted(aTask(ACTIVE, "1"), aTask(COMPLETED, "2"));

        ToDoMVC.edit("2", "2_edited");
        ToDoMVC.assertTasksAre("2_edited");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testEditByClickOutside() {
        ToDoMVC.givenAtCompleted(aTask(ACTIVE, "2"), aTask(COMPLETED, "1"));

        ToDoMVC.editByClickOutSide("1", "1_edited");
        ToDoMVC.assertTasksAre("1_edited");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDelete() {
        ToDoMVC.givenAtCompleted(aTask(COMPLETED, "1"), aTask(ACTIVE, "2"));

        ToDoMVC.delete("1");
        ToDoMVC.assertNoTasks();
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testFilteringAll() {
        ToDoMVC.given(aTask(COMPLETED, "1"), aTask(ACTIVE, "2"));

        ToDoMVC.filterCompleted();
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testReopenAll() {
        ToDoMVC.givenAtCompleted(aTask(COMPLETED, "1", "2"));

        ToDoMVC.toggleAll();
        ToDoMVC.assertNoTasks();
        ToDoMVC.assertItemsLeft(2);
    }

}
