package com.herokuapp.todomvc;

import com.herokuapp.todomvc.pages.ToDoMVC;
import com.herokuapp.todomvc.testconfigs.BaseTest;
import org.junit.Test;

import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.*;
import static com.herokuapp.todomvc.pages.ToDoMVC.aTask;


/**
 * Created by aru on 2017-03-28.
 */
public class OperationsAtActiveTest extends BaseTest{
   
    @Test
    public void testEdit() {
        ToDoMVC.givenAtActive(aTask(ACTIVE, "1", "2"));

        ToDoMVC.edit("2", "2_edited");
        ToDoMVC.assertTasksAre("1", "2_edited");
        ToDoMVC.assertItemsLeft(2);
    }

    @Test
    public void testEditByTab() {
        ToDoMVC.givenAtActive(aTask(ACTIVE, "1"));

        ToDoMVC.editByTab("1", "1_edited");
        ToDoMVC.assertTasksAre("1_edited");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testDelete() {
        ToDoMVC.givenAtActive(aTask(ACTIVE, "1"));

        ToDoMVC.delete("1");
        ToDoMVC.assertNoTasks();
        ToDoMVC.assertNoFooter();
    }

    @Test
    public void testComplete() {
        ToDoMVC.givenAtActive(aTask(ACTIVE, "1", "2"));

        ToDoMVC.toggle("2");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testClearCompleted() {
        ToDoMVC.givenAtActive(aTask(COMPLETED, "1"), aTask(ACTIVE, "2"));

        ToDoMVC.clearCompleted();
        ToDoMVC.assertTasksAre("2");
        ToDoMVC.assertItemsLeft(1);
    }

    @Test
    public void testReopenAll() {
        ToDoMVC.givenAtActive(aTask(COMPLETED, "1", "2"));

        ToDoMVC.toggleAll();
        ToDoMVC.assertTasksAre("1", "2");
        ToDoMVC.assertItemsLeft(2);
    }

    @Test
    public void testCancelEdit() {
        ToDoMVC.givenAtActive(aTask(ACTIVE, "1"), aTask(COMPLETED, "2"));

        ToDoMVC.cancelEdit("1", "1_edited");
        ToDoMVC.assertTasksAre("1");
        ToDoMVC.assertItemsLeft(1);
    }

}
