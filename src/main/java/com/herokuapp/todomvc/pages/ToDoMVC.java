package com.herokuapp.todomvc.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;
import java.util.StringJoiner;
import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static com.herokuapp.todomvc.pages.ToDoMVC.TaskStatus.*;

/**
 * Created by aru on 2017-03-28.
  */

//pagemodules approach
public class ToDoMVC {


    private static SelenideElement newTodo = $("#new-todo");

    private static SelenideElement itemsLeft = $("#todo-count>strong");

    private static ElementsCollection tasks = $$("#todo-list>li");

    @Step
    public static  void add(String... taskTexts) {
        for (String text : taskTexts) {
            newTodo.shouldBe(Condition.enabled).setValue(text).pressEnter();
        }
    }

    @Step
    public static  SelenideElement startEdit(String oldTaskText, String newTaskText) {

        tasks.find(Condition.exactText(oldTaskText)).doubleClick();

        SelenideElement inputEdit = tasks.findBy(Condition.cssClass("editing")).$(".edit");
        inputEdit.setValue(newTaskText);
        return inputEdit;
    }

    @Step
    public static  void edit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEnter();
    }

    @Step
    public static  void editByClickOutSide(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText);
        newTodo.shouldBe(Condition.enabled).click();
    }

    @Step
    public static  void cancelEdit(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressEscape();
    }

    @Step
    public static  void editByTab(String oldTaskText, String newTaskText) {
        startEdit(oldTaskText, newTaskText).pressTab();
    }

    @Step
    public static  void delete(String taskText) {
        tasks.find(Condition.exactText(taskText)).hover().$(".destroy").click();
    }

    @Step
    public static  void toggle(String taskText) {
        tasks.find(Condition.exactText(taskText)).$(".toggle").click();
    }

    @Step
    public static  void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public static  void clearCompleted() {
        $("#clear-completed").click();
    }


    @Step
    public static  void filterActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public static  void filterCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public static  void filterAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public static  void assertTasksAre(String... taskTexts) {
        tasks.filterBy(visible).shouldHave(exactTexts(taskTexts));
    }

    @Step
    public static  void assertNoTasks() {
        tasks.filterBy(visible).shouldBe(empty);
    }

    @Step
    public static  void assertItemsLeft(int count) {
        itemsLeft.shouldHave(Condition.exactText(Integer.toString(count)));
    }

    @Step
    public static  void assertNoFooter() {
        $("#footer").shouldNotBe(visible);
    }


    public static  enum TaskStatus {
        ACTIVE, COMPLETED
    }


    public static class Task {
        String text;
        TaskStatus status;

        Task(TaskStatus status, String text) {
            this.text = text;
            this.status = status;
        }

        public String toString() {
            String taskStatus = (status == ACTIVE) ? "false" : "true";
            return "{\\\"completed\\\":" + taskStatus + ", \\\"title\\\":\\\"" + text + "\\\"}";
        }

    }

    public static  void given(Task... tasks) {
        ensureUrlIsCorrect();

        StringJoiner addTasksJs = new StringJoiner(",", "localStorage.setItem(\"todos-troopjs\", \"[", "]\")");

        for (Task task : tasks) {
            addTasksJs.add(task.toString());
        }

        String result = addTasksJs.toString();

        executeJavaScript(result);
        refresh();
    }


    public static  void givenAtActive(Task... tasks) {
        given(tasks);
        filterActive();
    }

    public static  void givenAtCompleted(Task... tasks) {
        given(tasks);
        filterCompleted();
    }

    public static  Task[] aTask(TaskStatus taskStatus, String... taskTexts) {
        Task[] tasks = new Task[taskTexts.length];

        for (int i = 0; i < taskTexts.length; i++) {
            tasks[i] = new Task(taskStatus, taskTexts[i]);
        }

        return tasks;
    }

    public static  Task aTask(TaskStatus taskStatus, String taskText) {
        return new Task(taskStatus, taskText);
    }

    public static  void ensureUrlIsCorrect() {
        String baseURL = "https://todomvc4tasj.herokuapp.com";

        if (!baseURL.equals(url())) {
            open(baseURL);
        }
    }
}


